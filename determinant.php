<?php

function determinant($m) {
  $startTime = microtime(true);

  $det = new determinant();
  $det->m = $m;

  $det->getMinors();

  $result = $det->calculate($det);

  echo 'Elapsed time: ' . (microtime(true) - $startTime) . " seconds" . "\xA";

  return $result;
}

class determinant {
  public $m = [[]];
  public $value;
  public $factor = 1;
  public $minors;

  function calculate($det) {
    // TODO: not done, need to properly figure out how to initiate 

    if(count($det->m) == 2) {
      // return (ad - bc) the basic 2x2 determinant
      return (($det->m[0][0] * $det->m[1][1]) - ($det->m[0][1] * $det->m[1][0]));
    }
    else {
      // a * det(a_m) - b * det(b_m) - c * det(c_m)...
      $coloefficient = 1;
      $value = 0;

      for($i = 0; $i < count($det->minors); $i++) {
        $value += $coloefficient * $det->minors[$i]->factor * $this->calculate($det->minors[$i]);
        $coloefficient *= -1;
      }

      return $value;
    }
  }

  function getMinors() {
    $this->getMinors1($this);
  }

  function getMinors1(&$det) {
    $n = count($det->m);

    // base case is a 2 x 2 matrix which has no minors just return
    if($n == 2) {
      return;
    }

    // index variables for filling the minor
    $m_r = 0;
    $m_c = 0;

    // iterate through the first row of matrix values and gather minors for each factor
    for($i = 0; $i < $n; $i++){    
      $minor = new determinant();

      $minor->factor = $det->m[0][$i];

      // reset the minor row index to 0 at the start of each new factor
      $m_r = 0;

      for($row = 1; $row < $n; $row++){
        // reset the minor colum index to 0 at the start of each new row
        $m_c = 0;

        // increment minor row on each run except the first
        if($row > 1)
          $m_r++;

        // fill the minor matrix with the corresponding column values (i.e. those not in the same column)
        for($col = 0; $col < $n; $col++){
          if($col == $i)
            continue;
          else {
            $minor->m[$m_r][$m_c] = $det->m[$row][$col];
            $m_c++;
          }
        }
      }

      //recursively call getMinors
      $this->getMinors1($minor);

      // add the minor to the set of minors for this determinant
      $det->minors[] = $minor;
    }
  }

  // Print Functions
  function print() {
    echo 'value: ' . $this->value . "\xA";
    echo 'factor: ' . $this->factor . "\xA";
    echo 'minorsCount: ' . count($this->minors) . "\xA";
    printMatrix($this->m);
  }

  function printMinors() {
    $this->printMinors1($this);
  }

  function printMinors1($det) {
    if($det->minors != null || count($det->minors) != 0) {
      foreach($det->minors as $minor) {
        echo 'Factor: ' . $minor->factor . "\xA";
        printMatrix($minor->m);
        $this->printMinors1($minor);
      }
      echo '=========================' . "\xA";
    }
  }
}

function printMatrix($m) {

  $n = count($m);

  echo "n = " . $n . "\xA";

  for($row = 0; $row < $n; $row++) {
    for($col = 0; $col < $n; $col++) {
      echo $m[$row][$col] . " ";
    }
    echo "\xA";
  }
}

function testEquals($val1, $val2) {
  $val1 = round($val1, 5);
  $val2 = round($val2, 5);
  echo ($val1 == $val2 ? 'PASS - ' : 'FAIL - ') . 'Given: ' . $val1 . ' should be ' . $val2 . "\xA" . "\xA";
}

function test(){
  $n0_2 = array(
    array(1, 2),
    array(3, 4)
  );

  testEquals(determinant($n0_2), -2);

  $n0_3 = array(
    array(1,2,3),
    array(4,5,6),
    array(7,8,9)
  );

  testEquals(determinant($n0_3), 0);

  $n0_4 = array(
    array(1, 2, 3, 4),
    array(5, 6, 7, 8),
    array(9, 10, 11, 12),
    array(13, 14, 15, 16)
  );

  testEquals(determinant($n0_4), 0);

  $n3 = array(
    array(0.710018,0.31127,0.387354),
    array(0.0225254,0.133076,0.927198),
    array(0.800314,0.0823721,0.765086)
  );
  //det = 0.203141

  testEquals(determinant($n3), 0.203141);

  $n4 = array(
    array(0.483504,0.345927,0.328455,0.694491),
    array(0.161447,0.525948,0.217204,0.544596),
    array(0.0508131,0.815759,0.140718,0.248903),
    array(0.744744,0.428637,0.61251,0.877462)
  );
  //det = 0.0132982

  testEquals(determinant($n4), 0.0132982);

  $n5 = array(
    array(0.631967,0.385169,0.310225,0.491849,0.0917159),
    array(0.0911793,0.00328376,0.875235,0.353022,0.747208),
    array(0.420812,0.86065,0.82439,0.504244,0.431314),
    array(0.356104,0.840658,0.472832,0.833735,0.786879),
    array(0.855623,0.681525,0.747939,0.0974531,0.415146)
  );
  // det = -0.154846

  testEquals(determinant($n5), -0.154846);

  $n6 = array(
    array(0.205767,0.904352,0.968432,0.610875,0.00700682,0.980122),
    array(0.865093,0.300959,0.827513,0.758238,0.317002,0.956066),
    array(0.172665,0.015284,0.452961,0.0367898,0.726211,0.447743),
    array(0.221305,0.0137869,0.876352,0.172559,0.826931,0.642842),
    array(0.693174,0.117559,0.964004,0.392249,0.826348,0.315793),
    array(0.410522,0.681042,0.923631,0.244529,0.510848,0.526755)
  );
  // det: 0.019732

  testEquals(determinant($n6), 0.019732);

  $n6_2 = array(
    array(0.316428,0.981949,0.508287,0.294524,0.975963,0.0962398),
    array(0.117621,0.816363,0.216954,0.111121,0.316633,0.763315),
    array(0.285452,0.263745,0.783359,0.422988,0.680886,0.161912),
    array(0.873865,0.146569,0.12853,0.601903,0.973574,0.671635),
    array(0.076171,0.392049,0.996963,0.67192,0.230651,0.256787),
    array(0.333258,0.254812,0.544018,0.746651,0.345049,0.0372342)
  );
  // det = -0.00286174

  testEquals(determinant($n6_2), -0.00286174);

  $n7 = array(
    array(0.243491,0.623694,0.565106,0.461876,0.25518,0.859896,0.311821),
    array(0.398145,0.0505758,0.391954,0.601475,0.586829,0.150348,0.451844),
    array(0.0043907,0.450243,0.923548,0.102515,0.942143,0.443686,0.402263),
    array(0.528922,0.0547829,0.54896,0.157758,0.759135,0.766467,0.496084),
    array(0.48916,0.417033,0.153799,0.442757,0.863288,0.0879843,0.119021),
    array(0.405648,0.179587,0.163774,0.95862,0.679158,0.352724,0.154799),
    array(0.53263,0.63482,0.807458,0.129234,0.0998685,0.587462,0.203838)
  );

  // det = -0.0501749

  testEquals(determinant($n7), -0.0501749);

  $n8 = array(
    array(0.0992779,0.850991,0.0368816,0.279239,0.582968,0.928469,0.699374,0.731281),
    array(0.24019,0.536338,0.832462,0.385116,0.000705486,0.817459,0.366632,0.507383),
    array(0.773927,0.111445,0.49404,0.544585,0.0743985,0.533522,0.599775,0.556619),
    array(0.924699,0.781755,0.319477,0.761581,0.229387,0.846388,0.681347,0.185693),
    array(0.931512,0.513984,0.687527,0.98232,0.197323,0.235652,0.420583,0.11708),
    array(0.724968,0.369022,0.536587,0.190641,0.340777,0.0489566,0.621087,0.399676),
    array(0.573539,0.230174,0.690074,0.418449,0.629207,0.447114,0.981463,0.417273),
    array(0.204846,0.0828549,0.253766,0.617783,0.458765,0.454198,0.770488,0.68235)
  );

  // det = -0.0196861

  testEquals(determinant($n8), -0.0196861);

  //starts to get slow
  $n9 = array(
    array(0.502784,0.519543,0.523502,0.86012,0.436562,0.175102,0.66372,0.908611,0.618439),
    array(0.797799,0.212272,0.498878,0.517124,0.607472,0.540258,0.649597,0.690849,0.764321),
    array(0.103991,0.917648,0.358707,0.635079,0.618924,0.799814,0.0232757,0.91882,0.123535),
    array(0.809139,0.413036,0.657464,0.67072,0.215753,0.229826,0.652115,0.538161,0.674692),
    array(0.558121,0.646848,0.779111,0.894009,0.917661,0.909999,0.231178,0.197688,0.000130974),
    array(0.328404,0.976333,0.613274,0.664432,0.564514,0.536403,0.564669,0.51172,0.476633),
    array(0.0642046,0.455123,0.336101,0.218693,0.72384,0.661966,0.0171215,0.551222,0.293704),
    array(0.40585,0.289606,0.348623,0.204238,0.682198,0.674132,0.749832,0.780167,0.365473),
    array(0.330169,0.611311,0.429426,0.483833,0.953429,0.687152,0.288615,0.373419,0.506382)
  );

  // det = 0.00130994

  testEquals(determinant($n9), 0.00130994);

  $n10 = array(
    array(0.930392,0.827223,0.689928,0.802693,0.262208,0.574637,0.629579,0.248782,0.871623,0.0404302),
    array(0.552811,0.185122,0.807516,0.208821,0.0707999,0.48518,0.172272,0.150188,0.636555,0.652051),
    array(0.0642398,0.332729,0.741956,0.876608,0.668119,0.518165,0.348753,0.234981,0.00795615,0.0542873),
    array(0.118753,0.89686,0.376525,0.667596,0.577004,0.513443,0.777151,0.201797,0.584285,0.0527631),
    array(0.353353,0.344479,0.631987,0.893128,0.637314,0.925163,0.378064,0.914376,0.505715,0.636298),
    array(0.871305,0.340977,0.667992,0.713963,0.708195,0.96663,0.494084,0.58154,0.227033,0.705564),
    array(0.461199,0.0474034,0.22071,0.286972,0.234278,0.0182106,0.04724,0.145101,0.24637,0.273034),
    array(0.534347,0.651935,0.491439,0.209965,0.275479,0.0293267,0.216106,0.58461,0.331429,0.613507),
    array(0.602426,0.530446,0.165315,0.76008,0.0302045,0.946773,0.15516,0.724959,0.497856,0.750526),
    array(0.260651,0.0344924,0.558133,0.658838,0.901942,0.292281,0.758089,0.220017,0.762574,0.793678)
  );

  // det = -0.0173463

  // testEquals(determinant($n10), -0.0173463);

  $n0_12 = array(
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0,0,0,0,0)
  );

  // det = 0

  // testEquals(determinant($n0_12), 0);

  $n12 = array(
    array(0.223038,0.342752,0.456394,0.262114,0.965071,0.532456,0.400539,0.50949,0.0106824,0.201847,0.557072,0.448576),
    array(0.356152,0.0217994,0.398257,0.191329,0.560816,0.211668,0.19438,0.649209,0.0262478,0.556253,0.682708,0.0656409),
    array(0.640519,0.0888034,0.662362,0.213183,0.519583,0.326059,0.968135,0.934969,0.732426,0.872681,0.85344,0.0101049),
    array(0.257664,0.216246,0.320969,0.573253,0.354572,0.00568871,0.852735,0.770975,0.649143,0.594769,0.715436,0.708506),
    array(0.722384,0.55979,0.7143,0.641133,0.371979,0.287923,0.0219567,0.337545,0.759565,0.16332,0.756714,0.77262),
    array(0.253654,0.342232,0.737157,0.323591,0.659788,0.163765,0.602371,0.113035,0.850709,0.931039,0.410868,0.606612),
    array(0.461072,0.202002,0.180147,0.0771024,0.214791,0.913417,0.157735,0.785828,0.516134,0.419908,0.976256,0.100959),
    array(0.85985,0.712241,0.654239,0.652926,0.78866,0.828604,0.211341,0.667495,0.460038,0.128988,0.341552,0.981567),
    array(0.443536,0.547268,0.15859,0.0320034,0.594708,0.593855,0.922905,0.802008,0.791869,0.984348,0.602622,0.00732083),
    array(0.717533,0.335279,0.895576,0.426652,0.495264,0.727176,0.493441,0.381945,0.339674,0.220846,0.494989,0.452977),
    array(0.754677,0.0961736,0.90795,0.739319,0.918934,0.35904,0.755141,0.266862,0.842888,0.0301,0.704364,0.872847),
    array(0.779334,0.256605,0.223871,0.875306,0.841528,0.740352,0.914814,0.859736,0.668607,0.979142,0.0448963,0.235099)
  );
  //det = 0.014576

  // testEquals(determinant($n12), 0.014576);
}

test();